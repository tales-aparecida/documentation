---
---
<!-- markdownlint-disable first-line-heading -->

For merge request pipelines, related data is stored in various S3 buckets.
Details can be found on the [internal companion page].

[internal companion page]: https://documentation.internal.cki-project.org/docs/operations/removing-mr/
