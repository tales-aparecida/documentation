---
linkTitle: Presentations
Title: Talks and presentations
description: |
  The collected list of talks and presentations including links to videos and slides
weight: 30
---

## 2022

| Title                                                                             | Speakers                               | Links                                                                       |
|-----------------------------------------------------------------------------------|----------------------------------------|-----------------------------------------------------------------------------|
| [Advanced GitLab CI/CD for fun and profit][devconf22-p] *@ DevConf.CZ*            | [Iñaki Malerba], [Michael Hofmann]     | [Video][devconf22-v], [Slides][devconf22-s], [Source][devconf22-l]          |
| [Masking known issues across six kernel CI systems][fosdem22-p] *@ FOSDEM*        | [Nikolai Kondrashov]                   | [Video][fosdem22-v], [Slides][fosdem22-s]                                   |
| [Keynote panel discussion: 30 years of Linux][devconf22m-v-p] *@ DevConf.CZ Mini* | [Veronika Kabatova]                    | [Video][devconf22m-v-v]                                                     |
| [Open Source requirements for Services BoF][devconf22m-m-p] *@ DevConf.CZ Mini*   | [Michael Hofmann], [Stef Walter]       | [Video][devconf22m-m-v], [Slides][devconf22m-m-s], [Source][devconf22m-m-l] |
| [How the CKI team does estimation][agile22-s]                                     | [Veronika Kabatova], [Michael Hofmann] | [Slides][agile22-s], [Source][agile22-l]                                    |

## 2021

| Title | Speakers | Links |
|-------|----------|-------|
| Unifying Kernel Test Reporting with KernelCI *@ DevConf.CZ*| [Nikolai Kondrashov] | [Presentation](https://devconfcz2021.sched.com/event/gmMt/unifying-kernel-test-reporting-with-kernelci), [Video](https://www.youtube.com/watch?v=czHaqosibY0), [Slides](https://static.sched.com/hosted_files/devconfcz2021/00/Unifying%20Kernel%20Test%20Reporting%20with%20KernelCI%20-%20DevConf.cz%202021.pdf) |
| RHEL Kernel Workflow using GitLab *@ DevConf.CZ*| [Don Zickus], [Prarit Bhargava] | [Presentation](https://devconfcz2021.sched.com/event/gmN5/rhel-kernel-workflow-using-gitlab), [Video](https://www.youtube.com/watch?v=O9YRiXMskjU) |
| From Jenkins-under-your-desk to resilient service *@ DevConf.CZ*| [Iñaki Malerba], [Michael Hofmann] | [Presentation](https://devconfcz2021.sched.com/event/gmIL/from-jenkins-under-your-desk-to-resilient-service), [Video](https://www.youtube.com/watch?v=C7FLD0FUnsY), [Slides](https://static.sched.com/hosted_files/devconfcz2021/59/DevConf.cz%202021%20-%20From%20Jenkins-under-your-desk%20to%20resilient%20service.pdf) |
| UPt! Your Provisioning of Linux Machines *@ DevConf.CZ*| [Jakub Racek] | [Presentation](https://devconfcz2021.sched.com/event/gmOm/upt-your-provisioning-of-linux-machines), [Video](https://www.youtube.com/watch?v=gc2G8IqTIf0), [Slides](https://cki-project.org/news/2021-02-22-devconf-cz/2021-02-22-devconf-cz-upt-your-provisioning-of-linux-machines.odp) |
| Team Intro *@ Cyborg Infrastructure Workshop* | [Iñaki Malerba], [Michael Hofmann] | [Presentation](https://cki-project.org/news/2021-01-14-infra-workshop/), [Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-cki-team-intro.odp), [Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-cki-team-intro.pdf) |
| How the CKI team keeps its service running *@ Cyborg Infrastructure Workshop* | [Iñaki Malerba], [Michael Hofmann] | [Presentation](https://cki-project.org/news/2021-01-14-infra-workshop), [Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-run-your-service.odp), [Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-run-your-service.pdf) |
| How the CKI team hacks on its service *@ Cyborg Infrastructure Workshop* | [Iñaki Malerba], [Michael Hofmann] | [Presentation](https://cki-project.org/news/2021-01-14-infra-workshop), [Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.odp), [Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.pdf) |

## 2020

| Title | Speakers | Links |
|-------|----------|-------|
| Abusing GitLab CI to Test Kernel Patches *@ FOSDEM* | [Nikolai Kondrashov] | [Presentation](https://archive.fosdem.org/2020/schedule/event/testing_abusing_gitlabci_test_kernel), [Video](https://www.youtube.com/watch?v=56QzKNxen0M) |
| Plugging into the Red Hat kernel CI ecosystem *@ DevConf.CZ*| [Don Zickus] | [Presentation](https://devconfcz2020a.sched.com/event/YOvL), [Video](https://www.youtube.com/watch?v=KhdDtGZc_jY) |
| 7 Ways to Make Kernel Developers Like Cookies *@ DevConf.CZ* | [Jakub Racek], [Iñaki Malerba] | [Presentation](https://devconfcz2020a.sched.com/event/YOqY/7-ways-to-make-kernel-developers-like-cookies), [Video](https://www.youtube.com/watch?v=hNxcuWkvzpo), [Slides](https://static.sched.com/hosted_files/devconfcz2020a/9a/3-30-pm-7-ways.pdf) |

## 2019

| Title | Speakers | Links |
|-------|----------|-------|
| Red Hat joins CI party, brings cookies *@ Linux Plumbers Conference* | [Nikolai Kondrashov], [Veronika Kabatova] | [Presentation](https://lpc.events/event/4/contributions/287/), [Video](https://youtu.be/IM_k5fUsKhA) |
| Cookies for Kernel Developers *@ DevConf.CZ* | [Major Hayden], [Nikolai Kondrashov] | [Presentation](https://devconfcz2019.sched.com/event/7f9283e801b3690bd347635c97bf876f), [Video](https://www.youtube.com/watch?v=9KwDWsAqivo), [Slides](https://static.sched.com/hosted_files/devconfcz2019/68/Cookies%20for%20Kernel%20Developers.pdf) |

## 2018

| Title | Speakers | Links |
|-------|----------|-------|
| Kernel CI - How Red Hat can help *@ DevConf.CZ*| [Don Zickus] | [Presentation](https://devconfcz2018.sched.com/event/DJXI/kernel-ci-how-red-hat-can-help), [Video](https://www.youtube.com/watch?v=UYBu13CBmo8) |

[Don Zickus]: https://gitlab.com/dzickusrh
[Jakub Racek]: https://gitlab.com/jracek
[Iñaki Malerba]: https://gitlab.com/inakimalerba
[Major Hayden]: https://gitlab.com/majorhayden
[Michael Hofmann]: https://gitlab.com/mh21
[Nikolai Kondrashov]: https://gitlab.com/spbnick
[Prarit Bhargava]: https://gitlab.com/prarit
[Stef Walter]: https://gitlab.com/stefwalter
[Veronika Kabatova]: https://gitlab.com/veruu

[devconf22-p]: https://devconfcz2022.sched.com/event/siHv/advanced-gitlab-cicd-for-fun-and-profit
[devconf22-v]: https://www.youtube.com/watch?v=77913RmZUKk
[devconf22-s]: https://static.sched.com/hosted_files/devconfcz2022/79/DevConf.cz%202022%20-%20Advanced%20GitLab%20CI_CD%20for%20fun%20and%20profit.pdf
[devconf22-l]: https://docs.google.com/presentation/d/1-K93FEX8tAIlUHwQo4pAyEbp1czs59JYl7ZygVUrdMI

[fosdem22-p]: https://fosdem.org/2022/schedule/event/masking_known_issues_across_six_kernel_ci_systems
[fosdem22-v]: https://video.fosdem.org/2022/D.cicd/masking_known_issues_across_six_kernel_ci_systems.webm
[fosdem22-s]: https://fosdem.org/2022/schedule/event/masking_known_issues_across_six_kernel_ci_systems/attachments/slides/4882/export/events/attachments/masking_known_issues_across_six_kernel_ci_systems/slides/4882/Masking_known_issues_across_six_kernel_CI_systems_FOSDEM_2022.pdf

[devconf22m-v-p]: https://devconfczmini2022.sched.com/event/11lTy/keynote-panel-discussion-30-years-of-linux
[devconf22m-v-v]: https://www.youtube.com/watch?v=xob6ku7LB98

[devconf22m-m-p]: https://devconfczmini2022.sched.com/event/11l6G/open-source-requirements-for-services-bof
[devconf22m-m-v]: https://www.youtube.com/watch?v=BzlWqw_QW5w
[devconf22m-m-s]: https://static.sched.com/hosted_files/devconfczmini2022/84/Open%20Source%20Services%20Scorecard%20-%20engineering%20perspective.pdf
[devconf22m-m-l]: https://docs.google.com/presentation/d/1_mHzY_4cN1ekArFfHlK5YriTgQyql7aRhKfFd6D8320

[agile22-s]: 2022-agile-estimations.pdf
[agile22-l]: https://docs.google.com/presentation/d/1xQX2nxDLdV3YsxIP2Eyl84606H5Ys1kfkSzDzJsGASI
