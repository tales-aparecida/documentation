---
title: Testing CKI Builds
description: How to test CKI builds and submit results
weight: 25
---

## Ready for test messages

CKI sends ready for test messages after a kernel is built *and* after CKI
functional testing completes. This is meant to be used to trigger testing in
specialized labs for testing that is not feasible to onboard into CKI (e.g.
testing that requires a controlled environment or special hardware).

### Ready for test details

Messages are sent to `/topic/VirtualTopic.eng.cki.ready_for_test`. All labs
involved in extra testing will be notified if the topic is changed.

Onboarded labs can filter specific messages they are interested in. Some
examples include:

* Filtering on modified files to only test changes to their subsystem
* Filtering on `system/os` to only test changes to specific release
* Filtering on `cki_finished == false` to run testing in parallel
* Filtering on `cki_finished == true && status == 'success'` to e.g. only run
  performance tests if CKI functional testing passed
* Filtering on nonempty `patch_urls` to only test proposed changes instead of
  already merged ones
* A combination of the above

### Ready for test message schema

```json
{
    "ci": {
      "name": "CKI (Continuous Kernel Integration)",
      "team": "CKI",
      "docs": "https://cki-project.org/",
      "url": "https://xci32.lab.eng.rdu2.redhat.com/",
      "email": "cki-project@redhat.com",
      "irc": "#kernelci"
    },
    "run": {
      "url": "<PIPELINE_URL>"
    },
    "artifact": {
      "type": "cki-build",
      "issuer": "<PATCH_AUTHOR_EMAIL or CKI>",  # Always CKI for merge request testing
      "component": "kernel-source-package-name",
      "variant": "kernel-binary-package-name"
    },
    "system": [
      {
        "os": "<kpet_tree_family>",
        "stream": "<kpet_tree_name>"
      }
    ],
    "checkout_id": "<KCIDB checkout_id>",
    "build_info": [
      {
        "architecture": "<ARCH>",
        "build_id": "<KCIDB build_id>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      {
        "architecture": "<ARCH>",
        "build_id": "<KCIDB build_id>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      ...
    ],
    "patch_urls": ["list", "of", "strings", "or", "empty", "list"],
    "merge_request": {
      "merge_request_url": "link-or-empty-string",
      "is_draft": bool,
      "subsystems": ["list", "of", "strings"],
      "jira": ["list", "of", "jira", "links"],
      "bugzilla": ["list", "of", "bz", "links"]
    },
    "branch": "name-of-branch-or-empty-string",
    "modified_files": ["list", "of", "strings", "or", "empty", "list"],
    "pipelineid": <ID>,
    "cki_finished": bool,
    # status NOT present if cki_finished is false
    "status": <"success" or "fail">,
    "category": "kernel-build",
    "namespace": "cki",
    "type": "build",
    "generated_at": "<DATETIME_STRING>",
    "version": "0.1.0"
}
```

For documentation of `kpet_tree_family` and `kpet_tree_name`, see [Configuration
options]

### Installing the kernel from these messages

The CKI kernels can be installed for testing in Beaker via the [kpkginstall]
task:

```xml
<job>
  <recipeSet>
    <recipe ks_meta="redhat_ca_cert">
      <distroRequires>
        <distro_arch op="=" value="x86_64"/>
        <variant op="=" value="BaseOS"/>
        <distro_family op="=" value="CentOSStream9"/>
      </distroRequires>
      <hostRequires/>
      <task name="Kernel installation" role="STANDALONE">
        <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-tests-main.zip#distribution/kpkginstall"/>
        <params>
          <param name="KPKG_URL" value="{{build_info.kernel_package_url}}#package_name={{artifact.variant}}&amp;source_package_name={{artifact.component}}&amp;debug_kernel={{build_info.debug_kernel}}"/>
        </params>
      </task>
    </recipe>
  </recipeSet>
</job>
```

The `distroRequires` configuration needs to be adjusted based on the
`build_info.architecture`, `system.os` and `system.variant` fields. For debug
builds, it is also possible to use the proper variant name including the
`-debug` suffix instead of the `debug_kernel` parameter.

[kpkginstall]: https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/tree/main/distribution/kpkginstall

## Results messages

Test results should be sent back via UMB to
`/topic/VirtualTopic.eng.cki.results`. Results must be sent in KCIDB v4 format,
which has a detailed schema documented by [kcidb-io].

[Configuration options]: ../user_docs/configuration.md
[kcidb-io]: https://github.com/kernelci/kcidb-io/blob/main/kcidb_io/schema/v4.py
