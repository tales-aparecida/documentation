---
title: "About CKI"
description: General information about the CKI project
---

## Mission

Prevent bugs from being merged into kernel trees by providing CI-as-a-service.

## General information

* Code repositories: [gitlab.com/cki-project]
* Issue tracker: [gitlab.com/cki-project][issues]
* Documentation: [cki-project.org]
* Mailing list: [cki-project@redhat.com]

{{% include "internal.md" %}}

[gitlab.com/cki-project]: https://gitlab.com/cki-project/
[issues]: https://gitlab.com/groups/cki-project/-/issues
[cki-project.org]: https://cki-project.org/
[cki-project@redhat.com]: mailto:cki-project@redhat.com
