---
title: "Working with issues"
description: >
    How the CKI team is using GitLab issues
weight: 40
---

## General idea

GitLab issues are used to track planned and unplanned work and [incidents] in a
Kanban-like process.

Ideally, issues should represent the smallest deliverable
changes.

For each work item, a GitLab issue should be created. This

- creates visibility and enables reporting
- allows to attach merge requests to it, which provides a way to later track
  down all the work required for a certain work item

## Issue life cycle

Any issue created in any of the projects in the [cki-project] group on GitLab
will be automatically assigned an issue type if not done so during creation:

- `CWF::Type::ARK` for [kernel-ark], handled by the ARK team
- `CWF::Type::KWF` for [kernel-webhooks], handled by the Kernel Workflow team
- `CWF::Type::CKI` for all the rest

Additionally, `CWF::Type::Incident` is available for [incident
issues][incidents].

### Backlog refinement

The [CKI Refinement board] is used during daily stand-up to review newly
created tickets in the `Open` column. Once done, issues are dragged into the
`CWF::Refined` column.

Daily review of these issues ensures that

- everybody is aware of the work items and can ask clarifying questions and
  discuss prioritization
- knowledge transfer is facilitated by allowing everybody to raise their hand
  to work on an item, and request help and mentorship directly if they feel
  they do not have enough experience to resolve the issues on their own

### Kanban

Issues go through the following stages visible on the [CKI Kanban board].

#### Open

The issue has been created and is sitting peacefully in the backlog.

#### CWF::Stage::On Deck

The team has agreed on working on the issue in the next couple of weeks.

Issues are moved to this stage based on bi-weekly backlog review meetings or
(exceptionally) stand-up discussions.

Issues should be fleshed out enough to be implementable and have
clear acceptance criteria to limit scope creep. If necessary, bigger issues
should be split into the smallest deliverable changes.

For the bigger picture, issues can also be converted into epics, or tracked as
user stories. How all of this would work is still a bit unclear 🙈.

#### CWF::Stage::In Progress

The issue is actively worked on.

#### Closed

The issue is completed and all acceptance criteria are fulfilled.

If some acceptance criteria are remaining, but no further work is planned for
now, the outstanding acceptance criteria can also be moved into a new issue.
Alternatively, the issue can be moved back into the backlog.

[incidents]: incidents.md
[cki-project]: https://gitlab.com/cki-project/
[kernel-ark]: https://gitlab.com/cki-project/kernel-ark/
[kernel-webhooks]: https://gitlab.com/cki-project/kernel-webhooks/
[CKI Refinement board]: https://gitlab.com/groups/cki-project/-/boards/5484316
[CKI Kanban board]: https://gitlab.com/groups/cki-project/-/boards/933116

## Working with GitLab issues

### Creating a new issue

Create a new [GitLab issue][create-issue], e.g.

- on the top bar on a project page, select the plus sign (`+`) and then, under
  `This project`, select `New issue` from the top bar ("+")
- on the left sidebar on a project page, select `Issues` and then, in the
  upper-right corner, select `New Issue`
- on a project page, press the `i` shortcut
- on any of the GitLab issue boards, select the appropriate list
  menu (`⋮`) and then `Create new issue`

[create-issue]: https://docs.gitlab.com/ee/user/project/issues/create_issues.html

### Transitioning an issue between the different stages

To transition an issue to a different stage (`<STAGE>`), e.g.

- on the right sidebar on an issue page, select `Edit` next to `Labels`, and
  then select the appropriate `CWF::Stage::<STAGE>` label
- in the comment box on an issue page (`e` shortcut), write `/label
  ~"CWF::Stage::<STAGE>"` and submit the comment
- on any of the GitLab issue boards, drag the issue card to the appropriate
  list

### Closing an issue

Close the [GitLab issue][close-issue], e.g.

- at the top of an issue page, select `Close issue`
- in the comment box on an issue page (`e` shortcut), write `/close` and submit
  the comment
- on any of the GitLab issue boards, drag the issue card to the `Closed` list

[close-issue]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#close-an-issue

## Further ideas

In the future, it would be neat to track customer-promised issues somehow, as
these _have to_ be completed by a certain date, e.g. via a label.

<!-- vi: set spell spelllang=en: -->
