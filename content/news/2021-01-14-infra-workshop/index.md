---
title: "Cyborg Infrastructure Workshop"
type: blog
date: "2021-01-14"
author: Michael Hofmann
tags:
  - presentations
---

End of last week, we took part in the very first Cyborg Infrastructure Workshop
here at Red Hat. It was great to see so many faces again after such a long
time, even if just virtual.

The Cyborg group is part of the bigger effort to automate and modernize as much
as possible of the RHEL development and release process. The workshop had
presentations from the teams working on [Anaconda], [Cockpit], [Image Builder],
[OSCI], [Packit] and [Testing Farm].

Here are the presentations we gave:

- [Team Intro] ([pdf](2021-cyborg-infra-workshop-cki-team-intro.pdf))
- [How the CKI team keeps its service running] ([pdf](2021-cyborg-infra-workshop-how-do-you-run-your-service.pdf))
- [How the CKI team hacks on its service] ([pdf](2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.pdf))

[Team Intro]: 2021-cyborg-infra-workshop-cki-team-intro.odp
[How the CKI team keeps its service running]: 2021-cyborg-infra-workshop-how-do-you-run-your-service.odp
[How the CKI team hacks on its service]: 2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.odp

[Anaconda]: https://github.com/rhinstaller/
[Cockpit]: https://cockpit-project.org/
[Image Builder]: https://www.osbuild.org/
[OSCI]: https://github.com/fedora-ci/
[Packit]: https://packit.dev/
[Testing Farm]: https://gitlab.com/testing-farm/
